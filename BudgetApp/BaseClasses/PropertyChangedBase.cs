﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BudgetApp.BaseClasses
{
    public class PropertyChangedBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T Storage, T Value, [CallerMemberName] string PropertyName = null)
        {
            if (Equals(Storage, Value))
            {
                return false;
            }

            Storage = Value;
            OnPropertyChanged(PropertyName);
            return true;
        }

        protected void OnPropertyChanged(string PropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
