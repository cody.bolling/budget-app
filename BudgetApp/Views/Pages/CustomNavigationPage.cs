﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using NavigationPage = Xamarin.Forms.NavigationPage;

namespace BudgetApp.Views.Pages
{
    public class CustomNavigationPage : NavigationPage
    {
        public CustomNavigationPage(ContentPage Page) : base(Page)
        {
            On<iOS>().SetHideNavigationBarSeparator(true);
            SetDynamicResource(BackgroundColorProperty, "BackgroundColor");
            SetDynamicResource(BarBackgroundColorProperty, "ForegroundColor");
            SetDynamicResource(BarTextColorProperty, "TextPrimaryColor");
        }
    }
}
